// DCODR JavaScript Utilities
// By Shane Houstein
// Copyright DCODR 2013. 

(function($) {
	if($==null) return;
	window.dcodr = {};
	var dc = window.dcodr; //shortcut
	
	dc.namespace = function() {
		var a=arguments, o=null, i, j, d;
		for (i=0; i<a.length; i=i+1) {
			d=a[i].split(".");
			o=window;
			for (j=0; j<d.length; j=j+1) {
				o[d[j]]=o[d[j]] || {};
				o=o[d[j]];
			}
		}
    	return o;
	};
	
	dc.init = function() {
		var file = new dcodr.file.File("http://localhost/sites/default/files/christmas-surprise-ipad-wallpaper.jpeg");
		file.bind(dcodr.file.events.COMPLETE,function(){
			dc.alert('<img src="'+file.image.src+'">');
			console.log(file);
		});
		
		file.bind(dcodr.file.events.ERROR,function(){
			dc.alert("The file has failed");
			console.log(file);
		});
		file.load();

	};
	
	$(function() {
		dc.init();
	});
	
	dc.alert = function(obj) {
		var dump;
		if(typeof obj=="object" || typeof obj=="array") {
			dump = JSON.stringify(obj, null, "   ");
		} else {
			dump = obj;
		}
		if($.ui && $.ui.dialog) {
			if(document.getElementById('dcodr_alert')==null) {
				$(document.createElement('div')).attr("title","Alert!").attr('id','dcodr_alert').appendTo("body");
			}
			if(dump.replace) {
				dump = dump.replace("   ","&nbsp;&nbsp;&nbsp;","g");
				dump = dump.replace("\n","<br/>","g");
			}
			$("#dcodr_alert").html(dump).dialog();
		} else {
			alert(dump);
		}
	};
	
	//file utils
	var df = dc.namespace("dcodr.file");
		
	df.events = {
					COMPLETE:"dcodrFileLoaded",
					ERROR:"dcodrFileFailed"
				};

	
	//File Class (use new keyword)
	df.File = function(path, type) {
		if(type==null) {
			var parts = path.split(".");
			this.type = parts.pop();
		} else {
			this.type = type;
		}
		this.complete = false;
		this.path = path;
		this.eventHandler = $(this);
		this.loading = false;
	};
	
	df.File.prototype.load = function() {
		if(this.loading) return;
		this.loading = true;
		
		var that = this;
		
		var success = function() {
			that.loading = false;
			that.complete = true;
			that.trigger(df.events.COMPLETE);
		};
	
		var fail = function() {
			that.loading = false;
			that.complete = false;
			that.trigger(df.events.ERROR);
		};
	
		if(this.type=="js") {
			loadScript(this,success,fail);
		} else if(this.type=="css") {
			loadStyle(this,success,fail);
		} else if(this.type=="jpg" || this.type=="png" || this.type=="gif" || this.type=="jpeg") {
			loadImage(this,success,fail);
		}
	};
	
	var loadStyle = function(file,success,fail) {
		$.ajax({
			url:file.path,
			dataType:"text",
			success:function(data){
				if(document.createStyleSheet) {
					var styleSheet = document.createStyleSheet ("");
            		styleSheet.cssText = data;
				} else {
					$("head").append("<style>" + data + "</style>");
				}
				success();
			},
			error:function() {
				fail();
			}
		});
	};
	
	var loadScript = function(file,success,fail) {
		$.ajax({
			url: file.path,
			dataType: "script",
			success:function(data){
				success();
			},
			error:function() {
				fail();
			}
		});
	};
	
	var loadImage = function(file,success,fail) {
		var image = new Image;
		image.onload = function() {
			if ('naturalHeight' in this) {
				if (this.naturalHeight + this.naturalWidth === 0) {
					this.onerror();
					return;
				}
			} else if (this.width + this.height == 0) {
				this.onerror();
				return;
			}
			// At this point, there's no error.
			file.image = $("<img>").attr("src",image.src).get(0);
			success();
		};
		image.onerror = function() {
			file.image = null;
			fail();
		};
		image.src = file.path;
	};
	
	df.File.prototype.bind = function() {
		this.eventHandler.bind.apply(this.eventHandler,arguments);
	};
	
	df.File.prototype.unbind = function() {
		this.eventHandler.unbind.apply(this.eventHandler,arguments);
	};
	
	df.File.prototype.trigger = function() {
		this.eventHandler.trigger.apply(this.eventHandler,arguments);
	};
	
	
	
	//Object utils
	var ob = dc.namespace("dcodr.object");
	ob.isString = function(o) {
    	return typeof o == "string" || (typeof o == "object" && o.constructor === String);
	};
	
	/*
		Deletes all the properties from all objects given as parameters
		
		example: deleteProperties(obj1,obj); would remove all properties from obj1 and obj2
	*/
	ob.deleteProperties = function() {
		for (var a = 0; a< arguments.length; a++) {
			var obj = arguments[a];
			for(var i in obj) {
				delete obj[i];
			}
		}
	};
	
	/*
		Find an object by name.
		To establish scope, the first qualifier in the path (the part before the first '.') 
		should be mapped to a named object reference in rootMapping.
		
		Example dcodr.object.find("this.document.head",{"this":window})
	*/
	ob.find = function(pathStr,rootMapping) {
		var defaults = {"window":window,"dcodr":dcodr,"document":document};
		if(rootMapping==null) rootMapping = {};
		$.extend(rootMapping,defaults);
		
		var targs = pathStr.split(".");
		var obj = rootMapping[targs[0]];
		if(obj!=null) {
			targs.splice(0,1);
		} else {
			obj = window;
		}
		
		var targs = pathStr.split(".");
		var i=0;
		while(targs.length>0 && i<200 && obj!=null) {
			i++;
			obj = obj[targs[0]];
			targs.splice(0,1);
		}
		return obj;
	};
	
	
	/*
		Can call a method on any object by using a simple messaging format.
		A method call is described as a target name (t), a method name (m), and an array of params (p) to feed to the method.
		Method calls can be stored as JSON. example {"t":"document","m":"alert","p":["hello world"]}
	*/
	ob.callMethod = function(cmd,rootMapping) {
		//find the element by its path. The method shall be executed on the sprite if the "this" keyword is the first qualifier
		var obj = ob.find(cmd.t,rootMapping);
		
		//if we have an object, see if it has a method called <m> then force obj to call that method on itself, passing the params as an array
		if(obj!=null) {
			if(obj[cmd.m]!=null && typeof(obj[cmd.m])=="function") {
				obj[cmd.m].apply(obj, cmd.p);
			}
		}
	}
	
	//array utilities
	var a = dc.namespace("dcodr.array");
	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(elt, from) {
			var len = this.length;
		
			var from = Number(arguments[1]) || 0;
			from = (from < 0)
				 ? Math.ceil(from)
				 : Math.floor(from);
			if (from < 0)
			  from += len;
		
			for (; from < len; from++) {
			  if (from in this &&
				  this[from] === elt)
				return from;
			}
			return -1;
		}
	};

	a.removeAt = function(arr,from, to) {
		var rest = arr.slice((to || from) + 1 || arr.length);
		arr.length = from < 0 ? arr.length + from : from;
		return arr.push.apply(arr, rest);
	};
	
	a.removeValue = function(arr,value, start) {
		var i = arr.indexOf(value,start);
		if(i<=0) arr.splice(i, 1);
	};
	
	a.empty = function(arr) {
		while(arr.length>0) {
			arr.pop();
		}
	};
	
})(jQuery);
