<?php

/**
 * Project:  	dcodr
 * File:        RestRequest.class.php
*/

class RestRequest
{
  protected $url;
  protected $verb;
  protected $requestBody;
  protected $requestLength;
  protected $username;
  protected $password;
  protected $contentType;
  protected $acceptType;
  protected $userAgent;
  protected $response;
  protected $raw;
  
  public function __construct ($url = null, $verb = 'GET', $requestBody = null)
  {
    $this->url            = $url;
    $this->verb           = $verb;
    $this->requestBody    = $requestBody;
    $this->requestLength  = 0;
    $this->contentType    = 'application/json; charset=utf-8';
    $this->acceptType     = 'application/json';
	$this->userAgent = "ShaneTestApp (shane@pixellight.com.au)";
    $this->raw   = null;
    $this->response   = array();    
  }
  
  public function flush ()
  {
    $this->requestBody    = null;
    $this->requestLength  = 0;
    $this->verb        = 'GET';
    $this->response    = array();
    $this->raw    = null;
  }
  
  public function execute()
  {
    $ch = curl_init();
    $this->setAuth($ch);
    
    try
    {
      switch (strtoupper($this->verb))
      {
        case 'GET':
          $this->executeGet($ch);
          break;
        case 'POST':
          $this->executePost($ch);
          break;
        case 'POSTFILE':
          $this->executePostFile($ch);
          break;
        case 'PUT':
          $this->executePut($ch);
          break;
        case 'DELETE':
          $this->executeDelete($ch);
          break;
        default:
          throw new InvalidArgumentException('Current verb (' . $this->verb . ') is an invalid REST verb.');
      }
    }
    catch (InvalidArgumentException $e)
    {
      curl_close($ch);
      throw $e;
    }
    catch (Exception $e)
    {
      curl_close($ch);
      throw $e;
    }
    return $this->response;
  }
    
  protected function executeGet ($ch)
  {
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.$this->acceptType, 'Content-Type: '.$this->contentType,'User-Agent: '.$this->userAgent));    
    $this->doExecute($ch);  
  }
  
  protected function executePost ($ch)
  {    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.$this->acceptType, 'Content-Type: '.$this->contentType, 'User-Agent: '.$this->userAgent));
    
    $this->doExecute($ch);  
  }

  protected function executePostFile ($ch)
  {    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.$this->acceptType, 'Content-Type: '.$this->contentType,'User-Agent: '.$this->userAgent));
    curl_setopt($ch, CURLOPT_POST, 1);
    
    $this->doExecute($ch);      
  }
  
  protected function executePut ($ch)
  {
    $this->requestLength = strlen($this->requestBody);
    
    $fh = fopen('php://memory', 'rw');
    fwrite($fh, $this->requestBody);
    rewind($fh);
    
    curl_setopt($ch, CURLOPT_INFILE, $fh);
    curl_setopt($ch, CURLOPT_INFILESIZE, $this->requestLength);
    curl_setopt($ch, CURLOPT_PUT, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.$this->acceptType, 'Content-Type: '.$this->contentType,'User-Agent: '.$this->userAgent));
    
    $this->doExecute($ch);
    
    fclose($fh);
  }
  
  protected function executeDelete ($ch)
  {
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: '.$this->acceptType, 'Content-Type: '.$this->contentType));
    
    $this->doExecute($ch);
  }
  
  protected function doExecute (&$curlHandle)
  {
    $this->setCurlOpts($curlHandle);
    $response_str = curl_exec($curlHandle);
    $responseInfo = curl_getinfo($curlHandle);
	
	$header_str = substr($response_str,0,$responseInfo['header_size']);
	$content_str = substr($response_str,$responseInfo['header_size']);

	$this->response['content'] = json_decode($content_str, true);
	$this->response['info'] = $responseInfo;
	$this->response['http_code'] = $responseInfo['http_code'];
	$this->response['header'] = array();

	$lines = explode("\n",$header_str);

	foreach($lines as $line)
	{
		if (strpos($line, ": ") !== FALSE)
		{
			list($key,$value) = explode(": ",$line);
			$this->response['header'][$key] = trim($value);
		}
	}
	
	
    curl_close($curlHandle);
  }
  
  protected function setCurlOpts (&$curlHandle)
  {
    curl_setopt($curlHandle, CURLOPT_TIMEOUT, 10);
    curl_setopt($curlHandle, CURLOPT_URL, $this->url);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlHandle, CURLOPT_HEADER, true);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, !preg_match("!^https!i",$this->url));
  }
  
  protected function setAuth (&$curlHandle)
  {
    if ($this->username !== null && $this->password !== null)
    {
      curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
    }
  }
  
  public function getAcceptType ()
  {
    return $this->acceptType;
  } 
  
  public function setAcceptType ($acceptType)
  {
    $this->acceptType = $acceptType;
  } 
  
  public function getPassword ()
  {
    return $this->password;
  } 
  
  public function setPassword ($password)
  {
    $this->password = $password;
  } 
  
  public function getResponseBody ()
  {
    return $this->responseBody;
  } 
  
  public function getResponseInfo ()
  {
    return $this->responseInfo;
  } 
  
  public function getUrl ()
  {
    return $this->url;
  } 
  
  public function setUrl ($url)
  {
    $this->url = $url;
  } 
  
  public function getUsername ()
  {
    return $this->username;
  } 
  
  public function setUsername ($username)
  {
    $this->username = $username;
  } 
  
  public function getVerb ()
  {
    return $this->verb;
  } 
  
  public function setVerb ($verb)
  {
    $this->verb = $verb;
  } 

  public function getRequestBody ()
  {
    return $this->requestBody;
  } 
  
  public function setRequestBody ($body)
  {
    $this->requestBody = $body;
  } 

}

?>