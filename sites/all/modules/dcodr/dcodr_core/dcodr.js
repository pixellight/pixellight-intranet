var DCODR = {};
window.DCODR = DCODR;

(function($,context) {
	
	/*
		Helper to create a namespace within any JS object
		will not overwrite existing properties
	*/
	var createNamespace = function(a,o) {
		d=a.split(".");
		if(o==null) o=window;
		for (j=0; j<d.length; j=j+1) {
			o[d[j]]=o[d[j]] || {};
			o=o[d[j]];
		}
    	return o;
	};

	context.createNameSpace = createNamespace;

	/* Requires Underscore Library */
	//is this a value type (i.e. basic datatype - not object or array or undefined or null)
	var isValue = function(obj) {
		return(_.isString(obj) || _.isNumber(obj) || _.isBoolean(obj) || _.isDate(obj));
	};
	
	context.isValue = isValue;
	
	/* Requires Underscore Library */
	//underscore lacks a typeof function that is inline with its _is<type> functions
	dc.getType = function(obj) {
		if(_.isString(obj)) return "string";
		if(_.isNumber(obj)) return "number";
		if(_.isBoolean(obj)) return "boolean";
		if(_.isDate(obj)) return "date";
		if(_.isNull(obj)) return "null";
		if(_.isUndefined(obj)) return "undefined";
		if(_.isArray(obj)) return "array";
		return "object";
	};
	
	context.getType = getType;
	
	
	/* 	
 		---------------------
 		EventHandler (Object)
 		---------------------
  	*/
 	var EventHandler = function(){
    	this.listeners = {};
	};

	EventHandler.prototype.willTrigger = function(type){
 	   return this.listeners[type] != null;
	};
	
	EventHandler.prototype.bind = function(type, callback, options){
		if(!this.willTrigger(type)) {
			this.listeners[type] = [];
		}
		if(options==null) options={};
		this.listeners[type].push({callback:callback,options:options});
	};
	
	EventHandler.prototype.bindOnce = function(type, callback,options) {
		var i=0;
		if(!this.willTrigger(type)) {
			this.listeners[type] = [];
		}
		var stack = this.listeners[type];
		for(i = 0, l = stack.length; i < l; i++){
			if(stack[i].callback === callback){
				//console.log("rejected");
				return;
			}
		}
		if(options==null) options={};
		options.once = true;
		this.bind(type,callback,options);
	};
	
	EventHandler.prototype.unbind = function(type, callback){
		var i;
		if(!this.willTrigger(type)) {
			return;
		}
		var stack = this.listeners[type];
		for(i = 0, l = stack.length; i < l; i++){
			if(stack[i].callback === callback){
				stack.splice(i, 1);
				return this.unbind(type, callback);
			}
		}
	};
	
	EventHandler.prototype.trigger = function(type, options){
		var i;
		if(!this.willTrigger(type)) {
			return;
		}
		if(options==null) options={};
		options.type = type;
		options.target = this;
		var stack = this.listeners[type];
		var callbacks = [];
		var callOptions, callFunc;
		var removal = [];
    	for(i = 0, l = stack.length; i < l; i++) {
			callbacks.push(stack[i]);
			//stack[i].apply(this, [options]);
 		}
 		for(i = 0; i < callbacks.length; i++) {
 			callOptions = callbacks[i].options;
 			_.extend(callOptions,options);
 			callFunc = callbacks[i].callback;
			callFunc.apply(this, [callOptions]);
			if(callOptions.once) {
				removal.push({type:type,callback:callFunc});
			}
 		}
 		for(i=0; i<removal.length; i++) {
 			//console.log("removing one call event");
 			this.unbind(removal[i].type,removal[i].callback);
 		}
 	};
	
	context.EventHandler = EventHandler;
	
})(jQuery,DCODR);
