<?php
// $Id: block.tpl.php,v 1.4.2.3 2011/02/06 22:47:17 andregriffin Exp $
?>
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	<?php if (!empty($block->subject)): ?>
		<h2 <?php print $title_attributes; ?>><?php print $block->subject ?></h2>
	<?php endif;?>

	<div class="content"<?php print $content_attributes; ?>>
		<?php print $content ?>
	</div>

</div> <!-- /.block -->
