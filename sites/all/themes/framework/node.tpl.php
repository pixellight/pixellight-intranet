<?php
// $Id: node.tpl.php,v 1.4.2.4 2011/02/18 05:26:30 andregriffin Exp $
?>
<?php if (!$page): ?>
	<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php endif; ?>

<?php if ($user_picture || $display_submitted || !$page): ?>
	<?php if (!$page): ?>
		<div>
	<?php endif; ?>

		<?php print $user_picture; ?>

		<?php if (!$page): ?>
			<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
		<?php endif; ?>
		<?php if ($display_submitted): ?>
			<p class="submitted"><?php print $submitted; ?></p>
		<?php endif; ?>
	<?php if (!$page): ?>
		</div>
	<?php endif; ?>
<?php endif; ?>

	<div class="content"<?php print $content_attributes; ?>>
		<?php
			// Hide comments, tags, and links now so that we can render them later.
			hide($content['comments']);
			hide($content['links']);
			hide($content['field_tags']);
			print render($content);
		?>
	</div>

<?php if (!$page): ?>
	</div>
<?php endif; ?> <!-- /.node -->
