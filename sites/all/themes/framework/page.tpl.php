<?php
// $Id: page.tpl.php,v 1.4.2.6 2011/02/18 05:26:30 andregriffin Exp $

?>
<div id="wrapper" class="clearfix">

	<?php include("header.inc.php"); ?>

	<?php print render($page['page_top']); ?>

	<div id="main" role="main" class="clearfix">
		<?php if ($breadcrumb): print $breadcrumb; endif;?>
		<?php print $messages; ?>
		<a id="main-content"></a>
		<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
		<?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php endif; ?>
		<?php print render($page['content']); ?>
		<?php print render($page['after_content']); ?>
	</div> <!-- /#main -->

	<?php if ($page['sidebar_first']): ?>
		<div id="sidebar-first" role="complimentary" class="sidebar clearfix">
			<?php print render($page['sidebar_first']); ?>
		</div>	<!-- /#sidebar-first -->
	<?php endif; ?>

	<?php if ($page['sidebar_second']): ?>
		<div id="sidebar-second" role="complimentary" class="sidebar clearfix">
			<?php print render($page['sidebar_second']); ?>
		</div>	<!-- /#sidebar-second -->
	<?php endif; ?>

	<?php print render($page['page_bottom']); ?>

	<?php include("footer.inc.php"); ?>

</div> <!-- /#wrapper -->
