<div id="header" role="banner" class="clearfix">
	<?php if ($logo): ?>
		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
			<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		</a>
	<?php endif; ?>
	<?php if ($site_name || $site_slogan): ?>
		<div id="site-name-slogan">
			<?php if ($site_name): ?>
				<?php if ($title): ?>
					<div id="site-name"><strong>
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
					</strong></div>
				<?php else: /* Use h1 when the content title is empty */ ?>
					<h1 id="site-name">
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
					</h1>
				<?php endif; ?>
			<?php endif; ?>
			<?php if ($site_slogan): ?>
				<div id="site-slogan"><?php print $site_slogan; ?></div>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php print render($page['header']); ?>

	<?php if ($main_menu || $secondary_menu): ?>
		<div id="navigation" role="navigation" class="clearfix">
			<?php if ($page['navigation']): ?> <!--if block in navigation region, override $main_menu and $secondary_menu-->
				<?php print render($page['navigation']); ?>
			<?php endif; ?>
			<?php if (!$page['navigation']): ?>
				<?php if ($main_menu): print $main_menu; endif; ?>
				<?php if ($secondary_menu): print $secondary_menu; endif; ?>
			<?php endif; ?>
		</div> <!-- /#navigation -->
	<?php endif; ?>
</div> <!-- /#header -->